# raspberry-home-automation

This repository provides an automated install procedure of the raspberry pi 4 for home automation.
Features:
- Overlay file system on root partition
- Read-only boot partition
- JDownloader (incl. Oracle JDK)
- Samba server
- nginx webserver
- DLNA server
- Home Assistant
- MQTT mosquitto brooker
- Preparation of USB drive for the above services

# Installation

1. Download [Raspbian](https://www.raspberrypi.org/downloads/raspbian/). Direct link for always latest version: https://downloads.raspberrypi.org/raspbian_full_latest
2. Use [Win32DiskImager](https://www.heise.de/download/product/win32-disk-imager-92033) and format the RPI SD card with it.
3. Insert the SD card into the RPI and start it.
4. Follow the initial setup steps, including
   * For the user "pi" use the password "raspberry" (default)
   * Update of all packages
   * Configure wifi
   * Enable ssh and VNC access
5. Copy the entire repository to the RPI.
6. Connect a USB drive that shall store the downloaded files for media streaming.
7. Edit the file certificates.sh and put your passwords at the top.
8. Copy all private configurations/certificates into the config folder.
9. Run make the `install.sh` file executable and run `sudo ./install`.
   * This will set the looong password for the pi user :)
10. Reboot the RPI - everything should be ready.

# Usage

## ssh
Connect to `ssh pi@raspberrypi` and use the (looong) password of your choice :)

## vnc
Use [realvnc](https://www.realvnc.com/de/connect/download/viewer/) and connect to `raspberry`. In case you see the message "Cannot currently show the desktop" check https://www.tomshardware.com/how-to/fix-cannot-currently-show-desktop-error-raspberry-pi.
(raspi-config requires the boot partition to be in rw mode - check /etc/fstab!)

## FTP
Use the protocol `SFTP` and connect to the hostname `raspberry` (default port). Then use the credentials `pi` and the (looong) password of your choice.

## VPN
### Windows
Install these two certificates:
- VPNHostCert.pem
- CACert.pem

by running "mmc" (use WIN+r keys). Here, go to `File`->`Add/Remove Snap-in` and select "Certificates" from the left list. Then click "Add" and select "Computer account". Hit "Finish". In the mmc main window you will now see `Console root`->`Certificates (local computer)`. Here, go to "Trusted root certificates" and right click on the "Certificates" element below. Select "All Tasks" and then "Import". In the dialog, select the two mentioned certificates (run the dialog twice). Reboot the computer now.

Then, in Windows, create a new VPN configuration. Select "IKEv2", "Encryption: required" and as authentication use "Microsoft: EAP-MSCHAP v2".

## Overlay root file system

  If the overlay file system is enabled you can use the root partition as usual BUT CHANGES WILL NOT BE WRITTEN TO THE DISK!
  Hence, after a reboot all changes are lost.

  On login you will see information about the overlay status with motd:
  ```
  C:\Users\Martin>ssh pi@raspberrypi
  pi@raspberrypi's password:
  Linux raspberrypi 4.19.75-v7l+ #1270 SMP Tue Sep 24 18:51:41 BST 2019 armv7l

  ------ INFO: MOUNTED WITH OVERLAY ------

  The programs included with the Debian GNU/Linux system are free software;
  the exact distribution terms for each program are described in the
  individual files in /usr/share/doc/*/copyright.

  Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
  permitted by applicable law.
  Last login: Sat Dec 21 15:07:39 2019

  SSH is enabled and the default password for the 'pi' user has not been changed.
  This is a security risk - please login as the 'pi' user and type 'passwd' to set a new password.

  pi@raspberrypi:~ $
```

  For enabling/disabling the overlay use `overctl`:
  ```
pi@raspberrypi:~ $ overctl
Usage: overctl [-h|-r|-s|-t|-w]
   -h, --help     This message
   -r, --ro       Set read-only root with overlay fs
   -s, --status   Show current state
   -t, --toggle   Toggle between -r and -w
   -w, --rw       Set read-write root
```

  In order to make changes to the file system follow these steps:
  1. Disable the overlay after the next reboot: `sudo overctl -w`
  2. Reboot
  3. Make your changes to the root partition
  4. Enable the overlay again with `sudo overctl -r`
  5. Reboot again


## Boot partition
  The `/boot` partition will always be mounted in read-only mode. Overctl has no effect here. To make changes on the root partition use
  - `remount_boot_rw`

  or

  - `remount_boot_ro`

## JDownloader
Open https://my.jdownloader.org/ and login with the credentials you used for JDownloader on the RPI.

## Network mount (samba) - Downloads
To access the samba/smb server use these settings on Windows:

- Path: `\\raspberrypi\sambapi`
- User: pi
- raspberry

## DLNA
To access the DLNA streaming server use these apps/programs:
- [BubbleUPnP](https://play.google.com/store/apps/details?id=com.bubblesoft.android.bubbleupnp&hl=de)
- Windows Media Player

All of them should detect the RPI automatically.

## nginx
nginx has two configs active:

1. Access for files downloaded with JDownloader: To access the nginx server open http://raspberrypi:8080/ (no authentication required - not accessible from outside world).
2. Reverse-proxy for Home assistant - see below.

## Home Assistant
Access it with https://raspberrypi:30303/ (certificate invalid for this URL - use duckdns - see below!).

To make changes to the configuration log in to the RPI and go to `/home/homeassistant/.homeassistant`. The top level config file is `configuration.yaml`.

The log file is found at `/home/homeassistant/.homeassistant/home-assistant.log`.

Home Assistant can be started/stopped/restarted with 
```
sudo docker [start/stop/restart] homeassistant
```

### Download config to PC

Use the netdrive `\\raspberrypi\homeassistant` to copy configuration files to the computer.

Before and after copying, this command helps to provide the necessary file permissions:

Before copying:

```
sudo chmod 777 ./.storage/auth ./.storage/auth_provider.homeassistan
t  ./.storage/auth_module.totp ./.storage/core.restore_state ./.storage/core.device_registry ./.storage/core.config ./.s
torage/core.uuid
```


After copying:

```
sudo chmod 600 ./.storage/auth ./.storage/auth_provider.homeassistan
t  ./.storage/auth_module.totp ./.storage/core.restore_state ./.storage/core.device_registry ./.storage/core.config ./.s
torage/core.uuid
```


### Update Home Assistant
Use these commands:

```
sudo overctl -w
sudo reboot

sudo docker pull ghcr.io/home-assistant/home-assistant:stable
sudo docker stop homeassistant
sudo docker rm homeassistant
sudo service home-assistant@homeassistant start
```

### Google Assistant / DialogFlow
There are 2 integrations of Homie into the Google assistant:
1. Google Actions project "Homie Google Assistant"
2. Google Actions project "Dialogflow Homie"

The first one provides integration of the devices from Home assistant into the Google Home app. This allows for example to enable Lichti with "Hey Google, turn Lichti on".

The second one is necessary to automate things for which Google Home doesn't have an integration like the BMW. Important to know here is that the first account must be used as alpha version tester because this removes the message "this is a test version of ..." when talking to Google Assistant. 

Webhook of the Dialogflow project: https://home-martin-thien.duckdns.org/api/webhook/56d9509bea569267c1c13e7b3cbd999107439582ccfcdbaf13da4e4e45acbb11

### MariaDB
Home Assistant uses MariaDB instead of the default database. It is accessible with `sudo mysql -u root -p` (password: empty). The database "homeassistant" is used by the "homeassistant" user password: "homeassistant".

### homiedb
The homiedb script can be used to backup and restore the Home Assistant database when the raspberry shall be restarted:

1. [RPI is in RO mode]
2. Run `sudo homiedb -b` to back up the database to the sd-card
3. Disable overlay-fs and restarted: `sudo overctl -w && sudo reboot`
4. [RPI is in RW mode]
5. Run `sudo homiedb -r` to restore the database from the sd card into persistent filesystem
6. Do your work, enable overlay fs again and reboot normally - no need to use homiedb again now.

### DuckDNS / SSL certificate
The duckdns service provides easy access to the Home Assistant on the Raspberry Pi with https://home-martin-thien.duckdns.org - see the "Client certificate" section below.

A SSL certificate is used to secure the connection from the raspberry to DuckDNS. Information for the certificate and its generation: https://www.home-assistant.io/docs/ecosystem/certificates/lets_encrypt/

Certificate renewal is necessary every 90 days. This is done automatically and requires to have port 80 forwarded on the router.

To check if the certificate needs necessary, running

```
ssl-cert-check -b -c /etc/letsencrypt/live/home-martin-thien.duckdns.org/cert.pem | awk '{ print $NF }'
```

This prints the number of days until the certificate expires.

To renew the certificate manually, run 

```
/usr/local/bin/certbot-auto renew --no-self-upgrade --standalone --preferred-challenges http-01

sudo service nginx restart
```

### nginx reverse-proxy
A reverse proxy protects unauthorized access to Home Assistant. It is configured according to https://www.home-assistant.io/docs/ecosystem/nginx/ with these modifications:
- 4096 bits cipher instead of 2048 bits
- HTTP/2 enabled

The router must have port 443 forwarded.

### Client certificate
**Currently disabled because incompatible with various services**

A client certificate is required to access the home automation server on the Raspberry from the WAN. This certificate is generated automatically during installation and can be found at `/home/pi/certificates/home-martin-thien.pfx`.

All devices that shall access the home automation server require to have this certificate installed!

(The Home Assistant Android app currently does not support client certificates and thus can't be used.)

More information about the generation: https://gist.github.com/mtigas/952344

### MQTT brooker
A MQTT brooker (Mosquitto) is running on port 1883. The config file can be found at `/etc/mosquitto/mosquitto.conf`.

The client certificate is not used for communication because the ESP8266 doesn't seem to support it properly...

### Network mount (samba) - Home Assistant
To access the samba/smb server use these settings on Windows:

- Path: `\\raspberrypi\homeassistant`
- User: pi
- raspberry

## Changes to the install procedure

Commands can be added to the `install.sh` file and all `*.sh` files in the scripts folder to enhance the automated install.

**All commands must be save to execute repeatedly - always check if certain commands have already been executed before!**

# Security concept
1. Access from internet is only possible over port 443 (Home Assistant website) and port 80 (used every 90 days for DuckDNS certificate renewal). These ports must be open on the router.
2. The Raspberry has an nginx reverse-proxy installed that secures access to the Home Automation server from outside.
   1. The reverse-proxy uses an SSL certificate to secure connection to DuckDNS.
   2. The reverse-proxy uses secure authentication methods: HTTP/2, TLS1.2/1.3, only ciphers EECDH+AESGCM:EDH+AESGCM, HSTS and some more features.
   3. The reverse-proxy also verifies the client certificate which is required for all clients that want to connect.
   4. Checks with https://www.ssllabs.com/, https://www.htbridge.com/ssl/ and https://securityheaders.com/ done to enhance the results.
3. JDownloader does not require to have ports open.
4. User permissions
   1. Only the "pi" user has root permissions.
   2. Home Assistant runs with the "home-assistant" user (no root permissions).
   3. JDownloader runs with the "jdownloader" user (no root permissions).
5. The password for the "pi" user is changed to a strong one.
6. The sshd does not allow root logins ("PermitRootLogin no").
7. SSH connection from internet is not possible.
8. Home assistant settings used:
   1. cors_allowed_origins
   2. use_x_forwarded_for
   3. ip_ban_enabled
   4. login_attempts_threshold
9. Home assistant users use 2 factor authentication


# Updating the Raspberry Pi operating system
When updating the operating system (sudo apt-get upgrade) it is likely that there are changes to the kernel or modules close to the kernel/filesystem. In case the raspberry doesn't boot anymore, remove the SD card and put it into a computer.

Then have a look at the file /boot/cmdline.txt and remove the "boot=overlay" part at the front.

Reboot now.

If this is successful, you probably need to generate a new initramfs image with
```
KERNEL_VERSION=$(uname -r | cut -d'v' -f2 | cut -d'+' -f1)
remount_boot_rw
sudo update-initramfs -c -k $(uname -r)
sudo mv /boot/initrd.img-$(uname -r) /boot/initrd${KERNEL_VERSION}.img
```

Make sure, the file /boot/config.txt has the correct parameters for
- `kernel=kernel${KERNEL_VERSION}.img`
- `initramfs initrd${KERNEL_VERSION}.img`

The kernel version might have changed...

Reboot again. You should then be able to use the overctl tool to enable the overlay filesystem again.

# Fritz.box
Configure the fixed IP `192.168.178.45` for the raspberry pi.

Also, forward these ports:
- 80 (HTTP)
- 443 (HTTPS)
