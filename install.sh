#!/bin/bash -e

### Configurations ###

# change these lines first to what's configured on the RPI
RPI_USER="pi"
RPI_PASSWORD="raspberry"  # example

######################

DIRECTORY=$(cd `dirname $0` && pwd)

RED='\033[0;31m'
GREEN='\033[0;32m'
NO_COLOR='\033[0m' # No Color

ERROR="${RED}$(tput bold)"
SUCCESS="${GREEN}$(tput bold)"
NORMAL="${NO_COLOR}$(tput sgr0)"

if [ 'root' != $( whoami ) ] ; then
	echo -e "${ERROR}Please run as root!${NORMAL}"
	exit 1;
fi

if mount | grep -q -i "on / type .* (ro,"; then
	echo -e "${ERROR}Root partition is in read-only mode!${NORMAL}"
	exit 1;
fi

echo -e "${SUCCESS}Updating package sources ...${NORMAL}"
apt update || exit 1

chmod +x $DIRECTORY/scripts/*.sh

. $DIRECTORY/scripts/overlay-fs.sh
. $DIRECTORY/scripts/usb-media.sh
. $DIRECTORY/scripts/samba.sh
. $DIRECTORY/scripts/dlna.sh
. $DIRECTORY/scripts/nginx.sh
. $DIRECTORY/scripts/jdownloader.sh
. $DIRECTORY/scripts/home-assistant.sh
. $DIRECTORY/scripts/mqtt-brooker.sh
. $DIRECTORY/scripts/misc.sh

echo -e "${SUCCESS}Cleaning up ...${NORMAL}"
apt-get autoremove -y

echo -e ""
echo -e "${SUCCESS}Everything finished - reboot now!${NORMAL}"

exit 0
