#!/bin/bash

LOGFILE="/dev/null"
#LOGFILE="/home/pi/Desktop/network-monitor.log"
echo "Log file: $LOGFILE"

if ifconfig wlan0 | grep -q "inet" ; then
    echo "$(date "+%m %d %Y %T") : Ethernet OK" >> $LOGFILE
else
	echo "$(date "+%m %d %Y %T") : Ethernet connection down! Attempting reconnection." >> $LOGFILE
	#ifup --force wlan0
	service networking restart
	OUT=$? #save exit status of last command to decide what to do next
	if [ $OUT -eq 0 ] ; then
			STATE=$(ifconfig wlan0 | grep "inet addr:")
			echo "$(date "+%m %d %Y %T") : Network connection reset. Current state is" $STATE >> $LOGFILE
	else
			echo "$(date "+%m %d %Y %T") : Failed to reset ethernet connection" >> $LOGFILE
	fi
fi
echo "Network check done"
