#!/bin/bash

LOGFILE="/dev/null"
#LOGFILE="/home/pi/Desktop/jdownloader-monitor.log"
echo "Log file: $LOGFILE"

if pgrep -f "JDownloader" > /dev/null
then
    echo "JDownloader running." >> $LOGFILE
else
    echo "JDownloader stopped. Restarting it now." >> $LOGFILE
	service jdownloader restart
fi
echo "Jdownloader check done"
