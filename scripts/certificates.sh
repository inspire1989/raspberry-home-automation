#!/bin/bash -e

echo -e ""
echo -e "${SUCCESS}Certificates${NORMAL}"

echo -e "${SUCCESS}- Installing client SSL certificate ...${NORMAL}"
if [ ! -f /home/pi/certificates/ca.pem ]; then
	# run in subshell
	(
		set -e
		
		PRIVATE_CA_PASSWORD="PUT_YOUR_PASSWORD_HERE"
		PUBLIC_KEY="install_me"
		EXPORT_PASSWORD="PUT_ANOTHER_PASSWORD_HERE"

		# CLIENT_ID is only for filenames
		CLIENT_ID="home-martin-thien"
		CLIENT_SERIAL=01

		mkdir -p /home/pi/certificates
		chown pi:www-data /home/pi/certificates
		
		cd /home/pi/certificates

		# OPTION 1
		#echo "-- Setting up client-certificate signing CA (RSA)..."
		#echo "--- step 1"
		#openssl genrsa -aes256 -passout pass:${PRIVATE_CA_PASSWORD} -out ca.pass.key 4096
		#echo "--- step 2"
		#openssl rsa -passin pass:${PRIVATE_CA_PASSWORD} -in ca.pass.key -out ca.key
		#rm ca.pass.key
		
		# OPTION 2
		echo "-- Setting up client-certificate signing CA (EC)..."
		openssl ecparam -genkey -name secp521r1 | openssl ec -out ca.key
		
		# END OPTION
		
		echo "-- Generating the CA root certificate (valid for 100 years) ..."
		openssl req -new -x509 -days 36500 -key ca.key -out ca.pem -subj '/C=DE/ST=Hamburg/L=Hamburg/CN=Martin'

		# OPTION 1
		#echo "-- Creating a client keypair (RSA)..."
		#echo "--- step 1"
		#openssl genrsa -aes256 -passout pass:${PUBLIC_KEY} -out ${CLIENT_ID}.pass.key 4096
		#echo "--- step 2"
		#openssl rsa -passin pass:${PUBLIC_KEY} -in ${CLIENT_ID}.pass.key -out ${CLIENT_ID}.key
		#rm ${CLIENT_ID}.pass.key
		
		# OPTION 2
		echo "-- Creating a client keypair (EC)..."
		openssl ecparam -genkey -name secp521r1 | openssl ec -out ${CLIENT_ID}.key
		
		# END OPTION
		
		echo "--- CSR"
		openssl req -new -passin pass:${PUBLIC_KEY} -key ${CLIENT_ID}.key -out ${CLIENT_ID}.csr \
			-config $DIRECTORY/config/certificates/client-certificate/config.cnf

		echo "-- Signing this key with the CA (valid for 100 years)..."
		openssl x509 -req -days 36500 -in ${CLIENT_ID}.csr -CA ca.pem -CAkey ca.key \
			-set_serial ${CLIENT_SERIAL} -passin pass:${PRIVATE_CA_PASSWORD} -out ${CLIENT_ID}.pem \
			-extfile $DIRECTORY/config/certificates/client-certificate/v3.ext

		echo "-- Certificate information:"
		openssl x509 -text -noout -in ${CLIENT_ID}.pem

		echo "-- Generating crt file from pem file ..."
		openssl x509 -outform der -in ${CLIENT_ID}.pem -out ${CLIENT_ID}.crt

		#echo "-- Bundle the private key and certificate for end-user client ..."
		#cat ${CLIENT_ID}.key ${CLIENT_ID}.pem ca.pem > ${CLIENT_ID}.full.pem

		echo "-- Bundle client key into pfx file ..."
		openssl pkcs12 -export -out ${CLIENT_ID}.pfx -inkey ${CLIENT_ID}.key -in ${CLIENT_ID}.pem -certfile ca.pem \
			-password pass:${EXPORT_PASSWORD}

		echo "-- Checking certificate ..."
		openssl verify -CAfile ca.pem ${CLIENT_ID}.pem
		
		echo -e "${ERROR}-- Use this certificate for clients: /home/pi/certificates/${CLIENT_ID}.pfx !${NORMAL}"
		echo -e "${ERROR}-- Password for installation: ${EXPORT_PASSWORD}${NORMAL}"
	)
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"
