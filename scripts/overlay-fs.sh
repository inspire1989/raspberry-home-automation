#!/bin/bash -e

# see https://yagrebu.net/unix/rpi-overlay.md

echo -e ""
echo -e "${SUCCESS}Overlay root file system${NORMAL}"

echo -e "${SUCCESS}- Installing initramfs-tools ...${NORMAL}"
pkgs='initramfs-tools'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
	apt-get install -y $pkgs
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Modifying initramfs modules file ...${NORMAL}"
if ! grep -q overlay /etc/initramfs-tools/modules; then
	echo overlay >> /etc/initramfs-tools/modules
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Creating boot script ...${NORMAL}"
if [ ! -f /etc/initramfs-tools/scripts/overlay ]; then
	cp $DIRECTORY/config/overlay-fs/overlay /etc/initramfs-tools/scripts/overlay
	chmod +x /etc/initramfs-tools/scripts/overlay
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Creating initramfs image ...${NORMAL}"
KERNEL_VERSION=$(uname -r | cut -d'v' -f2 | cut -d'+' -f1)
if [ ! -f /boot/initrd${KERNEL_VERSION}.img ]; then
	update-initramfs -c -k $(uname -r)
	mv /boot/initrd.img-$(uname -r) /boot/initrd${KERNEL_VERSION}.img
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Adding initramfs image to boot config ...${NORMAL}"
if ! grep -q initrd${KERNEL_VERSION}.img /boot/config.txt; then
	echo "
kernel=kernel${KERNEL_VERSION}.img
initramfs initrd${KERNEL_VERSION}.img" >> /boot/config.txt
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Adding overlay boot parameter ...${NORMAL}"
if [ ! -f /boot/cmdline.txt.bak ]; then
	cp /boot/cmdline.txt /boot/cmdline.txt.bak
	echo -e "boot=overlay $(cat /boot/cmdline.txt)" > /boot/cmdline.txt
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting /boot as read-only in fstab ...${NORMAL}"
if [ 0 -eq $( grep -c 'defaults,ro' /etc/fstab ) ]; then
	sed -i.bak "/boot/ s/defaults/defaults,ro/g" /etc/fstab
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing remount_boot_X aliases ...${NORMAL}"
if [ 0 -eq $( grep -c 'remount_boot_' /etc/bash.bashrc ) ]; then
  cat $DIRECTORY/config/overlay-fs/bash.bashrc.addon >> /etc/bash.bashrc
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Creating overlay control script (overctl) ...${NORMAL}"
if [ ! -f /usr/local/sbin/overctl ]; then
	cp $DIRECTORY/config/overlay-fs/overctl /usr/local/sbin/overctl
	chmod +x /usr/local/sbin/overctl
	
	# original file without modifications
	cp /boot/cmdline.txt.bak /boot/cmdline.txt.orig
	
	# file that has overlay modifications
	cp /boot/cmdline.txt /boot/cmdline.txt.overlay
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing motd script (overctl) ...${NORMAL}"
if [ ! -f /etc/update-motd.d/80-overlay ]; then
	cp $DIRECTORY/config/overlay-fs/80-overlay /etc/update-motd.d/80-overlay
	chmod +x /etc/update-motd.d/80-overlay
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"