#!/bin/bash -e

echo -e ""
echo -e "${SUCCESS}Home Assistant${NORMAL}"

echo -e "${SUCCESS}- Setting up homeassistant user ...${NORMAL}"
if ! id -u homeassistant > /dev/null 2>&1; then
    useradd -rm homeassistant -G dialout,gpio,i2c
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing dependencies ...${NORMAL}"
pkgs='docker-ce fuse-overlayfs ssl-cert-check'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
	apt-get install fuse-overlayfs ssl-cert-check -y
	
	curl -sSL https://get.docker.com | sh
	
	usermod -aG docker homeassistant
	usermod -aG docker pi
	
	# enable fuse-overlayfs because the default overlay2 doesn't work inside another overlay fs
	echo '{
  "storage-driver": "fuse-overlayfs"
}' > /etc/docker/daemon.json
	
	systemctl enable docker
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing certbot-auto ...${NORMAL}"
if [ ! -f /usr/local/bin/certbot-auto ]; then
    mkdir -p /usr/local/bin
	
	cp -r $DIRECTORY/config/home-assistant/certbot-auto /usr/local/bin/certbot-auto
	chown root /usr/local/bin/certbot-auto
	chmod 0755 /usr/local/bin/certbot-auto
	
	# launch certbot-auto to make it download dependencies
	/usr/local/bin/certbot-auto --help
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing letsencrypt config ...${NORMAL}"
if [ ! -d /etc/letsencrypt/live/home-martin-thien.duckdns.org ]; then
    
	cp -r $DIRECTORY/config/home-assistant/letsencrypt /etc
	
	chmod -R 755 /etc/letsencrypt/live
	chmod -R 755 /etc/letsencrypt/archive
	
	ln -s /etc/letsencrypt/archive/home-martin-thien.duckdns.org/cert1.pem /etc/letsencrypt/live/home-martin-thien.duckdns.org/cert.pem 
	ln -s /etc/letsencrypt/archive/home-martin-thien.duckdns.org/chain1.pem /etc/letsencrypt/live/home-martin-thien.duckdns.org/chain.pem 
	ln -s /etc/letsencrypt/archive/home-martin-thien.duckdns.org/fullchain1.pem /etc/letsencrypt/live/home-martin-thien.duckdns.org/fullchain.pem 
	ln -s /etc/letsencrypt/archive/home-martin-thien.duckdns.org/privkey1.pem /etc/letsencrypt/live/home-martin-thien.duckdns.org/privkey.pem 
	
	# renew certificate now
	/usr/local/bin/certbot-auto renew --no-self-upgrade --standalone --preferred-challenges http-01
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up nginx reverse-proxy ...${NORMAL}"
if [ ! -f /etc/nginx/sites-available/home-assistant-reverse-proxy ]; then
	# install and enable new config
	cp $DIRECTORY/config/nginx/home-assistant-reverse-proxy /etc/nginx/sites-available/home-assistant-reverse-proxy
	ln -s /etc/nginx/sites-available/home-assistant-reverse-proxy /etc/nginx/sites-enabled/home-assistant-reverse-proxy
	
	if [ ! $( systemctl is-active --quiet nginx ) ]; then
		service nginx start
	fi

	service nginx reload
	service nginx restart
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing Home Assistant configuration ...${NORMAL}"
if [ ! -d /home/homeassistant/.homeassistant ]; then
	
	systemctl stop home-assistant@homeassistant || true
	
	# copy entire config folder and provide 755 permissions so that pi user can access for samba
	cp -r $DIRECTORY/config/home-assistant/.homeassistant /home/homeassistant
	chown -R homeassistant:homeassistant /home/homeassistant/.homeassistant
	chown -R homeassistant:homeassistant /home/homeassistant/.homeassistant
	chmod -R 777 /home/homeassistant/.homeassistant
	
	# user/passwords file has limited permissions
	chmod 600 /home/homeassistant/.homeassistant/.storage/auth_module.totp
	chmod 600 /home/homeassistant/.homeassistant/.storage/auth_provider.homeassistant
	chmod 600 /home/homeassistant/.homeassistant/.storage/auth
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing MariaDB ...${NORMAL}"
pkgs='mariadb-server libmariadb-dev-compat libmariadbclient-dev libssl-dev default-libmysqlclient-dev'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
	apt-get install -y $pkgs
	
	if [ ! $( systemctl is-active --quiet mariadb ) ]; then
		service mariadb start
	fi
	
	mysql -u root -p\n<<EOF
CREATE DATABASE homeassistant;
CREATE USER 'homeassistant' IDENTIFIED BY 'homeassistant';
ALTER USER 'homeassistant' IDENTIFIED WITH mysql_native_password BY 'homeassistant';
GRANT ALL PRIVILEGES ON homeassistant.* TO 'homeassistant';
FLUSH PRIVILEGES;
EXIT
EOF

    sudo -u homeassistant -H -s <<'EOF'
source /srv/homeassistant/bin/activate
pip3 install mysqlclient
EOF
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Fixing TLS version for BMW servers ...${NORMAL}"
if [ $(cat /etc/ssl/openssl.cnf | grep TLS | awk '{print $3}') != "TLSv1.0" ]; then
	# BMW servers only accept TLS 1.0 - see https://github.com/bimmerconnected/bimmer_connected/issues/111
	sed -i "s|TLSv1.2|TLSv1.0|g" /etc/ssl/openssl.cnf
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing and starting Home Assistant ...${NORMAL}"
if [ "$(docker inspect -f '{{.State.Running}}' homeassistant 2>/dev/null)" != "true" ]; then
	docker rm homeassistant 2>/dev/null || true
	
    docker run -d \
		--name homeassistant \
		--privileged \
		--restart=unless-stopped \
		-e TZ=Europe/Berlin \
		-v /home/homeassistant/.homeassistant:/config \
		-v /etc/letsencrypt:/etc/letsencrypt \
		--network=host \
		ghcr.io/home-assistant/home-assistant:stable
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing Home Assistant service ...${NORMAL}"
if [ ! -f /etc/systemd/system/home-assistant@homeassistant.service ]; then
	# this service allows to use docker in an overlay fs
    cp $DIRECTORY/config/home-assistant/docker-in-overlay.service /etc/systemd/system/docker-in-overlay.service
	
	# this service starts home assistant
    cp $DIRECTORY/config/home-assistant/home-assistant@homeassistant.service /etc/systemd/system/home-assistant@homeassistant.service
	
	systemctl --system daemon-reload
	
	systemctl enable docker
	
	systemctl enable docker-in-overlay
	systemctl start docker-in-overlay
	
	systemctl enable home-assistant@homeassistant
	systemctl start home-assistant@homeassistant
	
	# WAIT UNTIL HOME ASSISTANT IS FULLY STARTED BEFORE REBOOTING! DOWNLOADING THE DOCKER IMAGE TAKES SOME TIME AND IT ONLY WORKS WITHOUT OVERLAY FS!
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Creating database backup/restore script (homiedb) ...${NORMAL}"
if [ ! -f /usr/local/sbin/homiedb ]; then
	cp $DIRECTORY/config/home-assistant/homiedb /usr/local/sbin/homiedb
	chmod +x /usr/local/sbin/homiedb
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"