#!/bin/bash -e

NGINX_PATH=$USB_MEDIA_PATH/sambapi

echo -e ""
echo -e "${SUCCESS}nginx${NORMAL}"

echo -e "${SUCCESS}- Installing nginx ...${NORMAL}"
pkgs='nginx'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
	apt-get install -y $pkgs
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Cleaning up default config ...${NORMAL}"
if [ -f /etc/nginx/sites-available/default ]; then
    
	rm /etc/nginx/sites-available/default
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Disabling default config ...${NORMAL}"
if [ -f /etc/nginx/sites-enabled/default ]; then
    
	rm /etc/nginx/sites-enabled/default
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up nginx downloads config ...${NORMAL}"
if [ ! -f /etc/nginx/sites-available/downloads ]; then
	cp $DIRECTORY/config/nginx/downloads /etc/nginx/sites-available/downloads
	sed -i "s|NGINX_PATH|$NGINX_PATH|g" /etc/nginx/sites-available/downloads
	
	ln -s /etc/nginx/sites-available/downloads /etc/nginx/sites-enabled/downloads
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up dhparams ...${NORMAL}"
if [ ! -f /etc/nginx/ssl/dhparams.pem ]; then
	mkdir -p /etc/nginx/ssl
	
	# generate dhparams.pem
	#openssl dhparam -out /etc/nginx/ssl/dhparams.pem 4096
	
	# copy pre-computed dhparams.pem
	cp $DIRECTORY/config/nginx/dhparams.pem /etc/nginx/ssl/dhparams.pem
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up nginx config ...${NORMAL}"
if ! cmp --silent $DIRECTORY/config/nginx/nginx.conf /etc/nginx/nginx.conf; then
	cp $DIRECTORY/config/nginx/nginx.conf /etc/nginx/nginx.conf
	
	if [ ! $( systemctl is-active --quiet nginx ) ]; then
		service nginx start
	fi

	service nginx reload
	service nginx restart
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"