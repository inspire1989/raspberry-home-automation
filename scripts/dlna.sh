#!/bin/bash -e

MINIDLNA_PATH=$USB_MEDIA_PATH/sambapi

echo -e ""
echo -e "${SUCCESS}miniDLNA${NORMAL}"

echo -e "${SUCCESS}- Installing miniDLNA ...${NORMAL}"
pkgs='minidlna'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
	apt-get install -y $pkgs
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up minidlna config ...${NORMAL}"
if [ 0 -eq $( grep -c "$MINIDLNA_PATH" /etc/minidlna.conf ) ]; then
	chmod 755 /media
	chmod 755 /media/pi
	chmod 755 $USB_MEDIA_PATH
	chmod 755 $MINIDLNA_PATH
	
	cp $DIRECTORY/config/dlna/minidlna.conf /etc/minidlna.conf
	sed -i "s|MINIDLNA_PATH|$MINIDLNA_PATH|g" /etc/minidlna.conf
	
	service minidlna force-reload
	service minidlna start
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"