#!/bin/bash -e

JDOWNLOADER_PATH=$USB_MEDIA_PATH/jdownloader

echo -e ""
echo -e "${SUCCESS}JDownloader${NORMAL}"

# openJDK does not work!
echo -e "${SUCCESS}- Installing Oracle JDK ...${NORMAL}"
if [ ! -d /usr/java ]; then
	mkdir -p /usr/java || true
	tar xf $DIRECTORY/packages/jdk-8u231-linux-arm32-vfp-hflt.tar.gz -C /usr/java
	update-alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_231/bin/java 1
	update-alternatives --install /usr/bin/javac javac /usr/java/jdk1.8.0_231/bin/javac 1
	update-alternatives --set java /usr/java/jdk1.8.0_231/bin/java
	update-alternatives --set javac /usr/java/jdk1.8.0_231/bin/javac
	chmod a+x /usr/bin/java
	chmod a+x /usr/bin/javac
	java -version
	javac -version
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Downloading JDownloader.jar ...${NORMAL}"
if [ ! -f $JDOWNLOADER_PATH/JDownloader.jar ]; then
	mkdir -p $JDOWNLOADER_PATH
	wget -P $JDOWNLOADER_PATH http://installer.jdownloader.org/JDownloader.jar
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up jdownloader user ...${NORMAL}"
if ! id -u jdownloader > /dev/null 2>&1; then
    useradd -rm jdownloader
	
	# Prompt: Start JDownloader now?
	while true; do
		read -p "Jdownloader requires MyJDownloader and 9kw settings. Do you want to start JDownloader now and configure everything? [yN]" yn
		case $yn in
			[Yy]* ) 
				echo -e "Starting JDownloader... update takes a while. When finished set up MyJDownloader and 9kw login and set $SAMBAPI_PATH as download path!"
				echo -e "This script will continue when JDownloader exits. Close it after you are done."
				sudo -u jdownloader java -jar $JDOWNLOADER_PATH/JDownloader.jar || true
				echo -e "Done"
				break;;
			* ) break;;
		esac
	done
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Setting up JDownloader service ...${NORMAL}"
if [ ! -f /etc/systemd/system/jdownloader@jdownloader.service ]; then
	cp $DIRECTORY/config/jdownloader/jdownloader.service /etc/systemd/system/jdownloader@jdownloader.service
	sed -i "s|JDOWNLOADER_PATH|$JDOWNLOADER_PATH|g" /etc/systemd/system/jdownloader@jdownloader.service
	
	systemctl --system daemon-reload
	systemctl enable jdownloader@jdownloader
	systemctl start jdownloader@jdownloader
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"