#!/bin/bash -e

echo -e ""
echo -e "${SUCCESS}MQTT Brooker (Mosquitto)${NORMAL}"

echo -e "${SUCCESS}- Installing Mosquitto ...${NORMAL}"
pkgs='mosquitto mosquitto-clients'
if ! dpkg -s $pkgs >/dev/null 2>&1; then

	wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
	apt-key add mosquitto-repo.gpg.key
	
	wget http://repo.mosquitto.org/debian/mosquitto-stretch.list -P /etc/apt/sources.list.d/
	
	apt-get update
	
	apt-get install -y $pkgs
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing Mosquitto configuration ...${NORMAL}"
if ! diff -rq $DIRECTORY/config/mosquitto/mosquitto.conf /etc/mosquitto/mosquitto.conf ; then
	
	service mosquitto stop
	
	cp $DIRECTORY/config/mosquitto/mosquitto.conf /etc/mosquitto/mosquitto.conf
	chmod -R 644 /home/homeassistant/.homeassistant
	
	service mosquitto start
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing paho-mqtt ...${NORMAL}"
if ! pip list | grep -F paho-mqtt >/dev/null 2>&1; then

	pip install paho-mqtt
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"