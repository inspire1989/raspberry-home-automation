#!/bin/bash -e

echo -e ""
echo -e "${SUCCESS}Mounting USB media${NORMAL}"

NUM_USB_MEDIA_DEVICES=$( mount | grep '/media/pi' | cut -d'/' -f6 | cut -d' ' -f1 | wc -l )
USB_MEDIA_UUID=""
USB_MEDIA_PATH=""

if [ 1 -eq $NUM_USB_MEDIA_DEVICES ]; then
	USB_MEDIA_UUID=$( mount | grep '/media/pi' | cut -d'/' -f6 | cut -d' ' -f1 )
	USB_MEDIA_PATH=/media/pi/$USB_MEDIA_UUID
	echo -e "- USB media device found: $USB_MEDIA_PATH"

	mkdir -p /media/pi/$USB_MEDIA_UUID
	chown -R pi:pi /media/pi/$USB_MEDIA_UUID
	chmod -R 777 /media/pi/$USB_MEDIA_UUID
elif [ 0 -eq $NUM_USB_MEDIA_DEVICES ]; then
	echo -e "${ERROR}- No USB media devices found - exactly one must be connected!${NORMAL}"
	exit 1
else
	echo -e "${ERROR}- Multiple USB media devices found - exactly one must be connected!${NORMAL}"
	exit 1
fi

echo -e "${SUCCESS}- Enabling USB drive in fstab ...${NORMAL}"
if [ 0 -eq $( grep -c "$USB_MEDIA_PATH" /etc/fstab ) ]; then
	echo -e "
UUID=$USB_MEDIA_UUID /media/pi/$USB_MEDIA_UUID ext4 defaults,noatime,nofail 0
" >> /etc/fstab
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"