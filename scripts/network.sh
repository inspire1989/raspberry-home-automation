#!/bin/bash -e

echo "UNTESTED UNTESTED UNTESTED"
exit 1

echo -e ""
echo -e "${SUCCESS}Installing network monitor script${NORMAL}"

echo -e "${SUCCESS}- Copying network monitor script ...${NORMAL}"
if [ ! -f /home/pi/Desktop/network-monitor.sh ]; then
	cp $DIRECTORY/config/network/network-monitor.sh /home/pi/Desktop/network-monitor.sh
	chmod +x /home/pi/Desktop/network-monitor.sh
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"
