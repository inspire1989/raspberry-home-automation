#!/bin/bash -e

SAMBAPI_PATH=$USB_MEDIA_PATH/sambapi

echo -e ""
echo -e "${SUCCESS}samba${NORMAL}"

echo -e "${SUCCESS}- Installing samba ...${NORMAL}"
pkgs='samba samba-common-bin acl'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
	apt-get install -y $pkgs
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Creating SAMBA share folder ...${NORMAL}"
if [ ! -d $SAMBAPI_PATH ]; then
	mkdir -p $SAMBAPI_PATH
	chown -R pi.pi $SAMBAPI_PATH
	
	setfacl -Rdm group:pi:rwx $SAMBAPI_PATH
	setfacl -Rm group:pi:rwx $SAMBAPI_PATH
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing smbd config ...${NORMAL}"
if ! grep -q $SAMBAPI_PATH /etc/samba/smb.conf; then
	cp $DIRECTORY/config/samba/smb.conf /etc/samba/smb.conf
	mkdir -p /var/log/samba
	
	sed -i "s|SAMBAPI_PATH|$SAMBAPI_PATH|g" /etc/samba/smb.conf
	sed -i "s|SAMBAPI_USER|$RPI_USER|g" /etc/samba/smb.conf
	
	smbpasswd -s -a $RPI_USER << EOF
$RPI_PASSWORD
$RPI_PASSWORD
EOF

	if [ ! $( systemctl is-active --quiet smbd ) ]; then
		service smbd start
	fi

	service smbd reload
	service smbd restart
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}Finished!${NORMAL}"