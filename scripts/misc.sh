#!/bin/bash -e

echo -e ""
echo -e "${SUCCESS}Misc.${NORMAL}"

echo -e "${SUCCESS}- Preventing swap usage better ...${NORMAL}"
if [ $( cat /proc/sys/vm/swappiness ) != "1" ]; then
	sysctl vm.swappiness=1
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Increasing size of swap file ...${NORMAL}"
if [ $(cat /etc/dphys-swapfile | grep CONF_SWAPSIZE | awk -F= '{print $2}') != "2000" ]; then
	# stop swapping
	dphys-swapfile swapoff

	# set to 2GB (maximum allowed by RPI)
	sed -i "s|CONF_SWAPSIZE|CONF_SWAPSIZE=2000\n#|g" /etc/dphys-swapfile
	
	# re-initialize swapping
	dphys-swapfile setup
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Creating symlink to sdcard on desktop ...${NORMAL}"
if [ ! -L /home/pi/Desktop/sdcard-symlink ]; then
	ln -s $USB_MEDIA_PATH /home/pi/Desktop/sdcard-symlink
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Installing crontab for root ...${NORMAL}"
crontab -l > /tmp/tmp_crontab_root || true
if [ ! -f /var/spool/cron/crontabs/root ] ||
     ! diff -rq $DIRECTORY/config/misc/crontab_root /tmp/tmp_crontab_root ; then
	 
	crontab $DIRECTORY/config/misc/crontab_root
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Disabling ssh root login ...${NORMAL}"
if [ 0 -eq $( grep -c 'PermitRootLogin no' /etc/ssh/sshd_config ) ]; then
	echo "
PermitRootLogin no
" >> /etc/ssh/sshd_config
	
	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

echo -e "${SUCCESS}- Changing password for pi user ...${NORMAL}"
if [ ! -f /home/pi/.pwd_changed_do_not_remove_this_file ]; then
	echo "pi:MS19ter04ter1989" | chpasswd
	
	touch /home/pi/.pwd_changed_do_not_remove_this_file
	
	echo -e "   ${ERROR}PASSWORD FOR USER pi HAS BEEN CHANGED!!!${NORMAL}"

	echo -e "${SUCCESS}-- Finished${NORMAL}"
else
	echo -e "${ERROR}-- skipped${NORMAL}"
fi

# only required for network.sh which is currently disabled
#echo -e "${SUCCESS}- Installing crontab for pi ...${NORMAL}"
#sudo -u pi crontab -l > /tmp/tmp_crontab_pi
#if [ ! -f /var/spool/cron/crontabs/pi ] ||
#     ! diff -rq $DIRECTORY/config/misc/crontab_pi /tmp/tmp_crontab_pi ; then
#
#	crontab $DIRECTORY/config/misc/crontab_pi
#	
#	echo -e "${SUCCESS}-- Finished${NORMAL}"
#else
#	echo -e "${ERROR}-- skipped${NORMAL}"
#fi

echo -e "${SUCCESS}Finished!${NORMAL}"